//
//  XYPoint.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "XYPoint.h"

@implementation XYPoint

@synthesize x,y;

-(void) setX:(double)xVal andY:(double)yVal
{
    x=xVal;
    y=yVal;
}

@end
