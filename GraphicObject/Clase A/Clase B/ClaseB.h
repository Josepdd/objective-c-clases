//
//  ClaseB.h
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "ClaseA.h"

@interface ClaseB : ClaseA

-(void) initVar;
-(void) printVar;

@end
