//
//  ClaseB.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "ClaseB.h"

@implementation ClaseB

-(void) initVar
{
    x=200;
}
-(void) printVar
{
    NSLog(@"x = %i", x);
}

@end
