//
//  ClaseA.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "ClaseA.h"

@implementation ClaseA

-(void) initVar
{
    x=100;
}
-(void) printVar
{
    NSLog(@"x = %i", x);
}

@end
