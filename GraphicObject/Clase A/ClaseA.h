//
//  ClaseA.h
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClaseA : NSObject
{
    int x;
}

-(void) initVar;
-(void) printVar;

@end
