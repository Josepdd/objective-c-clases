//
//  Triangle.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "Triangle.h"

@implementation Triangle

@synthesize base, altura, lado1, lado2;

-(double) perimetro
{
    return base + lado1 + lado2;
}
-(double) area
{
    return (base*altura)/2;
}

@end
