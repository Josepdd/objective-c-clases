//
//  Triangle.h
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "GraphicObject.h"

@interface Triangle : GraphicObject

@property double base, altura, lado1, lado2;

-(double) perimetro;
-(double) area;

@end
