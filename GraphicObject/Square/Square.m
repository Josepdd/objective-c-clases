//
//  Square.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "Square.h"

@implementation Square : NSObject

-(Square *) initWithSide:(int)s
{
    self.side=s;
    return self;
}
-(int) setSide:(int)s
{
    [rect setWidth:s andHeight:s];
    return s;
}
-(int) side
{
    return (int)[rect width];
}
-(int) area
{
    return (int)[rect area];
}
-(int) perimeter
{
    return (int)[rect perimeter];
}

@end
