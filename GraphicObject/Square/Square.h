//
//  Square.h
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rectangle.h"

@interface Square: NSObject
{
    Rectangle *rect;
}
-(Square *) initWithSide: (int) s;
-(int) setSide: (int) s;
-(int) side;
-(int) area;
-(int) perimeter;

@end
