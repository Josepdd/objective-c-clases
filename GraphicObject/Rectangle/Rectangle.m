//
//  Rectangle.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "Rectangle.h"
#import "XYPoint.h"

@implementation Rectangle
{
    XYPoint *origin;
}
@synthesize width, height;

-(void) setOrigin:(XYPoint *)pt
{
    if (!origin) {
        origin = [XYPoint new];
    }
    origin.x = pt.x;
    origin.y = pt.y;
}
-(XYPoint *) origin
{
    return origin;
}
-(double) area
{
    return width * height;
}
-(double) perimeter
{
    return 2 * (width + height);
}
-(void) setWidth:(double)w andHeight:(double)h
{
    width = w;
    height = h;
}
-(void) translate:(XYPoint *)pt
{
    origin.x = pt.x;
    origin.y = pt.y;
}
-(BOOL) containsPoint:(XYPoint *)aPoint
{
    if (origin.x == aPoint.x && origin.y == aPoint.y) {
        return YES;
    }else{
        return NO;
    }
}
-(Rectangle *) intersect:(Rectangle *)r
{
    Rectangle *result = [Rectangle new];
    if (r.origin.x < origin.x) {
        if (r.origin.x + width > origin.x) {
            result.width = width-(origin.x - r.origin.x);
        }else{
            result.width = 0;
        }
    }else{
        result.width = 0;
    }
    if (r.origin.y < origin.y) {
        if (r.origin.y + height > origin.y) {
            result.height = height-(origin.y - r.origin.y);
        }else{
            result.height = 0;
        }
    }else{
        result.height = 0;
    }
    return result;
}

@end
