//
//  Circle.h
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "GraphicObject.h"

@interface Circle : GraphicObject

@property double radio;

-(double) area;
-(double) circunferencia;

@end
