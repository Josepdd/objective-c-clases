//
//  Circle.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "Circle.h"

@implementation Circle

@synthesize radio;

-(double) area
{
    return M_PI*radio*radio;
}
-(double) circunferencia
{
    return 2*M_PI*radio;
}

@end
