//
//  GraphicObject.h
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GraphicObject : NSObject
{
    int fillColor;  // color de 32 bits
    BOOL filled;    // ¿El objeto esta relleno?
    int lineColor;  // Color de la linea de 32 bits
}

-(int) fillColor;
-(int) lineColor;
-(BOOL) filled;
-(void) setFillColor: (int) c;
-(void) setLineColor: (int) c;
-(void) setFilled: (BOOL) f;

@end
