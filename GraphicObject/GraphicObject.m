//
//  GraphicObject.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "GraphicObject.h"

@implementation GraphicObject

-(int) fillColor
{
    return fillColor;
}
-(int) lineColor
{
    return lineColor;
}
-(BOOL) filled
{
    return filled;
}
-(void) setFillColor:(int)c
{
    fillColor = c;
}
-(void) setFilled:(BOOL)f
{
    filled = f;
}
-(void) setLineColor:(int)c
{
    lineColor = c;
}

@end
