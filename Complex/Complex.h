//
//  Complex.h
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Complex : NSObject

@property double real, imaginary;

-(void) setReal: (double) a andImaginary: (double) b;
-(void) print; // a + bi
-(Complex *) add: (Complex *) f;

@end
