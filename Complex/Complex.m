//
//  Complex.m
//  Clases
//
//  Created by Josep Dols on 25/2/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "Complex.h"

@implementation Complex

@synthesize real, imaginary;

-(void) print{
    NSLog(@"%f + %f i",real,imaginary);
}
-(void) setReal:(double)a andImaginary:(double)b
{
    real = a;
    imaginary = b;
}
-(Complex *) add:(Complex *)f
{
    Complex *result = [Complex new];
    
    result.real = real + f.real;
    result.imaginary = imaginary + f.imaginary;
    
    return result;
}
@end
